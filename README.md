# bedtools Singularity container
### Bionformatics package bedtools<br>
bedtools: a powerful toolset for genome arithmetic. Collectively, the bedtools utilities are a swiss-army knife of tools for a wide-range of genomics analysis tasks. The most widely-used tools enable genome arithmetic: that is, set theory on the genome. For example, bedtools allows one to intersect, merge, count, complement, and shuffle genomic intervals from multiple files in widely-used genomic file formats such as BAM, BED, GFF/GTF, VCF<br>
bedtools Version: 2.29.2<br>
[http://bedtools.readthedocs.org/]

Singularity container based on the recipe: Singularity.bedtools_v2.29.2

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bedtools_v2.29.2.sif Singularity.bedtools_v2.29.2`

### Get image help
`singularity run-help ./bedtools_v2.29.2.sif`

#### Default runscript: STAR
#### Usage:
  `bedtools_v2.29.2.sif --help`<br>
    or:<br>
  `singularity exec bedtools_v2.29.2.sif bedtools --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bedtools_v2.29.2.sif oras://registry.forgemia.inra.fr/gafl/singularity/bedtools/bedtools:latest`


